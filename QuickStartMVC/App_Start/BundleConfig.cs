﻿using System.Web;
using System.Web.Optimization;

namespace QuickStartMVC
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/JS/lib/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/JS/lib/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                        "~/JS/lib/jquery.dataTables.min.js",
                        "~/JS/lib/dataTables.bootstrap.min.js",
                        "~/JS/lib/dataTables.colReorder.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/CSS/bootstrap.min.css",
                      //"~/CSS/bootstrap-theme.min.css",
                      "~/CSS/jquery.dataTables.min.css",
                      "~/CSS/jquery.dataTables_themeroller.css",
                      "~/CSS/dataTables.bootstrap.min.css",
                      "~/CSS/colReorder.bootstrap.min.css",
                      "~/CSS/site.css"));
        }
    }
}
